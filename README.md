# Workshop 4: Speeding up Python

***DUE TO TIME CONSTRAINTS, THIS WORKSHOP WAS CANCELLED***

This is the fourth in a series of scientific Python lectures at Risø campus,
Denmark Technical University.

## Workshop objective

Discuss code profilling and how you can make long programs run more quickly.

## Who should come

Anyone interested in learning the different ways to understand why your code
is slow and how to make it faster. Previous Python experience not required.

## Date

The workshop date and location will be announced internally at DTU Risø. Please
use the contact information below for questions on the workshop contents or 
arranging a new workshop.

## Topic outline

- Cython
- Multiprocessing

## Prerequisites

If you are attending the workshop, please do the following before attending:
1. If you do not have Anaconda installed, please [install it](https://www.anaconda.com/download/)
**with Python 3.6**
2. If you have Anaconda installed, please either  
    a) have your root environment be Python 3.6, or  
    b) [create an environment](https://conda.io/docs/user-guide/tasks/manage-environments.html#creating-an-environment-with-commands)
    that has Python 3.6

## Contact

Jenni Rinker  
rink@dtu.dk